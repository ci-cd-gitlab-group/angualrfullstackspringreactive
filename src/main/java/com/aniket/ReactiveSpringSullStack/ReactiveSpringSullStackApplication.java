package com.aniket.ReactiveSpringSullStack;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;

@SpringBootApplication(exclude = MongoDataAutoConfiguration.class)
public class ReactiveSpringSullStackApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReactiveSpringSullStackApplication.class, args);
	}

}
