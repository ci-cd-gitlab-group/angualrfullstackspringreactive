package com.aniket.ReactiveSpringSullStack.config;

import com.mongodb.reactivestreams.client.MongoClient;
import de.flapdoodle.embed.mongo.spring.autoconfigure.EmbeddedMongoAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.ReactiveMongoDatabaseFactory;
import org.springframework.data.mongodb.core.ReactiveMongoOperations;
import org.springframework.data.mongodb.core.ReactiveMongoTemplate;
import org.springframework.data.mongodb.core.SimpleReactiveMongoDatabaseFactory;

@Configuration
@Profile(value = "local")
@Import(EmbeddedMongoAutoConfiguration.class)
public class DataConfig {

    public  static  final String DATABASE_NAME = "reservations";

    public ReactiveMongoDatabaseFactory reactiveMongoDatabaseFactory(MongoClient mongoClient){
        return new SimpleReactiveMongoDatabaseFactory(mongoClient , DATABASE_NAME);
    }

    public ReactiveMongoOperations reactiveMongoTemplate(ReactiveMongoDatabaseFactory reactiveMongoDatabaseFactory){
        return new ReactiveMongoTemplate(reactiveMongoDatabaseFactory);
    }
}
