package com.aniket.ReactiveSpringSullStack.controller;

import com.aniket.ReactiveSpringSullStack.model.Reservation;
import com.aniket.ReactiveSpringSullStack.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping(ReservationResource.ROOM_V_1_RESERVATION)
@CrossOrigin
public class ReservationResource {
    public static final String ROOM_V_1_RESERVATION = "/room/v1/reservation/";

    private final ReservationService reservationService;

    @Autowired
    public ReservationResource(ReservationService reservationService) {
        this.reservationService = reservationService;
    }


    @GetMapping(path = "{roomId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Reservation> getReservationById(@PathVariable String roomId) {
        return reservationService.getReservation(roomId);
    }


    @GetMapping(path = "", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public Flux<Reservation> getAllReservations() {
        return reservationService.listAllReservations();
    }


    @PostMapping(path = "", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Reservation> createReservation(@RequestBody Mono<Reservation> reservationDetails) {
        return reservationService.createReservation(reservationDetails);
    }

    @PutMapping(path = "{roomId}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Mono<Reservation> updateReservationPrice(@PathVariable String roomId, @RequestBody Mono<Reservation> reservationDetails) {
        return reservationService.updateReservation(roomId,reservationDetails);
    }

    @DeleteMapping(path = "{roomId}")
    public Mono<Boolean> deleteReservationById(@PathVariable String roomId) {
        return reservationService.deleteReservation(roomId);
    }


}
