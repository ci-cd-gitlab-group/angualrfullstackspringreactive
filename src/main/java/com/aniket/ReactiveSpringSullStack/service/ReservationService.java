package com.aniket.ReactiveSpringSullStack.service;

import com.aniket.ReactiveSpringSullStack.model.Reservation;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ReservationService {

    Mono<Reservation> getReservation(String roomId);

    Mono<Reservation> createReservation(Mono<Reservation> reservationMono);

    Mono<Reservation> updateReservation(String roomId, Mono<Reservation> reservationMono);

    Mono<Boolean> deleteReservation(String roomId);


    Flux<Reservation> listAllReservations();
}
